FROM condaforge/mambaforge

# set the (default) environment name
ARG ENV_NAME="test"

# import the environment file
COPY environment.yml /etc/environment.yml

# create the environment and update the bash configuration to
# automatically activate it
RUN mamba env create -n ${ENV_NAME} -q -f /etc/environment.yml && \
    echo "conda activate ${ENV_NAME}" >> /etc/skel/.bashrc && \
    echo "conda activate ${ENV_NAME}" >> ~/.bashrc

# copy the entrypoint script that automatically activates the env
COPY entrypoint.sh /etc/entrypoint.sh

# define the entrypoint to run the entrypoint script
ENTRYPOINT ["tini", "--", "/etc/entrypoint.sh"]
# use bash for cmd entry
CMD ["/bin/bash"]
