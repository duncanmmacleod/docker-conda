#!/bin/bash

. ${CONDA_DIR:=/opt/conda}/etc/profile.d/conda.sh
conda activate test
exec "$@"
